﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using NSwag.Annotations;
using ASECL.Models;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.IO;

namespace ASECL.Controllers
{
   
    [RoutePrefix("api/certs")]
    public class CertController : BaseApiController
    {

        private Cert FC2Data()
        {
            Cert data = new Cert();
            var Form = HttpContext.Current.Request.Form;
            var Files = HttpContext.Current.Request.Files;
            data.Content = Form.Get("Content");
            data.Title = Form.Get("Title");
            data.IsDisabled = Form.Get("IsDisabled") == "true";
            data.CreatedAt = DateTime.Now;
            data.UpdatedAt = DateTime.Now;
            try
            {
                string sortNum = Form.Get("SortNum");
                if (sortNum != "" )
                {
                    data.SortNum = int.Parse(sortNum);
                }
                
            } catch ( Exception err )
            {
                data.SortNum = 0 ;
            }
            if (Files["PdfFile"] != null)
            {
                string PdfPath = string.Format("/Uploads/Pdfs/{0}.pdf", Guid.NewGuid().ToString());
                Files["PdfFile"].SaveAs(string.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, PdfPath));
                data.File = PdfPath;
            }
            if ( Files["ImageFile"] != null )
            {
                FileInfo fi = new FileInfo(Files["ImageFile"].FileName);
                string ImageFilePath = string.Format("/Uploads/Images/{0}{1}", Guid.NewGuid().ToString(), fi.Extension);
                Files["ImageFile"].SaveAs(string.Format("{0}/{1}", AppDomain.CurrentDomain.BaseDirectory, ImageFilePath));
                data.Image = ImageFilePath;
            }

            return data;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult getList()
        {
            validToken();
            return Json(new ListResponse<Cert>(Db.Cert.OrderBy(x => x.SortNum).ThenBy(x => x.Id).ToList()));
        }

        [HttpPost]
        [Route("")]
        [OpenApiExtensionData("summary", "")]
        [Consumes("multipart/form-data")]
        public async Task<IHttpActionResult> Add()
        {
            validToken();
            try
            {
                
                Cert data = FC2Data();
                List<Cert> certList = Db.Cert.OrderBy(x => x.SortNum).ToList();
                try
                {
                    certList.Insert(data.SortNum - 1, data);
                }
                catch (Exception err)
                {
                    certList.Add(data);
                }
                
                int sortNum = 0;
                foreach ( var item in certList ) {
                    item.SortNum = ++sortNum;
                }

                Db.Cert.Add(data);
                Db.SaveChanges();
                return Json(new ResultResponse<Cert>(data));
            } catch ( Exception err )
            {
                return Json(new ErrorResponse("新增失敗"));
            }
            
        }

        /// <summary>
        /// 3213
        /// </summary>
        /// <param name="Id">2321</param>
        /// <param name="PdfFile">123</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{Id}")]
        [Consumes("multipart/form-data")]
        public async Task<IHttpActionResult> Update(int Id)
        {
            validToken();
            try
            {
                Cert data = FC2Data();
                try
                {
                    Cert dbData = Db.Cert.Where(x => x.Id == Id).First();

                    

                    dbData.Title = data.Title;
                    dbData.Content = data.Content;
                    dbData.IsDisabled = data.IsDisabled;
                    dbData.UpdatedAt = DateTime.Now;
                    if (data.Image != null)
                    {
                        dbData.Image = data.Image;
                    }
                    if (data.File != null)
                    {
                        dbData.File = data.File;
                    }


                    if (dbData.SortNum != data.SortNum)
                    {
                        List<Cert> certList = Db.Cert.Where(x => x.Id != Id).OrderBy(x => x.SortNum).ToList();
                        try
                        {
                            certList.Insert(data.SortNum - 1, dbData);
                        } catch ( Exception err )
                        {
                            certList.Add(dbData);
                        }
                        int sortNum = 0;
                        foreach( var item in certList)
                        {
                            item.SortNum = ++sortNum;
                        }
                    }


                    Db.SaveChanges();

                    return Json(new ResultResponse<Cert>(dbData));
                }
                catch (Exception err)
                {
                    return Json(new ErrorResponse("查無資料"));
                };
            } catch (Exception err )
            {
                return Json(new ErrorResponse("編輯異常"));
            }
            
        }

        [HttpDelete]
        [Route("{Id}")]
        public IHttpActionResult DelItem(int Id)
        {
            validToken();
            try
            {
                Cert dbData = Db.Cert.Where(x => x.Id == Id).First();
                List<Cert> items = Db.Cert.Where(x => x.Id != Id).ToList();
                int sortNum = 0;
                foreach(var item in items )
                {
                    item.SortNum = ++sortNum;
                }
                Db.Cert.Remove(dbData);
                Db.SaveChanges();

                return Json(new ResultResponse<Cert>(dbData));
            }
            catch (Exception err)
            {
                return Json(new ErrorResponse("查無資料"));
            }
        }
    }
}