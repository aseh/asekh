﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using NSwag.AspNet.Owin;
using Owin;

[assembly: OwinStartup(typeof(ASECL.Startup))]

namespace ASECL
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // 如需如何設定應用程式的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkID=316888
            var config = new HttpConfiguration();
            var root = AppDomain.CurrentDomain.BaseDirectory;

            //app.UseSwaggerUi3(typeof(Startup).Assembly, settings =>
            //{
            //    //針對RPC-Style WebAPI，指定路由包含Action名稱
            //    settings.GeneratorSettings.DefaultUrlTemplate =
            //        "api/{controller}/{action}/{id?}";
                
            //    //可加入客製化調整邏輯
            //    settings.PostProcess = document =>
            //    {
            //        document.Info.Title = "WebAPI 範例";
            //    };
            //});
            
            app.UseWebApi(config);

            app.UseStaticFiles();

            var FrontDefaultFilesOptions = new DefaultFilesOptions();
            FrontDefaultFilesOptions.DefaultFileNames = new List<string> { "index.html" };
            FrontDefaultFilesOptions.FileSystem = new PhysicalFileSystem(Path.Combine(root, "Content/Front"));
            
            app.UseDefaultFiles(FrontDefaultFilesOptions);
            var FrontStaticFilesOptions = new StaticFileOptions();
            FrontStaticFilesOptions.FileSystem = new PhysicalFileSystem(Path.Combine(root, "Content/Front"));

            app.UseStaticFiles(FrontStaticFilesOptions);

            var BackendStaticFilesOptions = new StaticFileOptions();
            BackendStaticFilesOptions.RequestPath = new PathString("/backend");
            BackendStaticFilesOptions.FileSystem = new PhysicalFileSystem(Path.Combine(root, "Content/Backend/build"));
            
            app.UseStaticFiles(BackendStaticFilesOptions);
            

            app.Use(async (context, next) =>
            {
                if ( context.Request.Path.ToString().Contains("/backend") )
                {
                    context.Response.ContentType = "text/html";
                    string backendHtml = File.ReadAllText(Path.Combine(root, "Content/Backend/build/index.html"));
                    string applicationPath = HttpContext.Current.Request.ApplicationPath == "/" ? "" : HttpContext.Current.Request.ApplicationPath;
                    string publicUrl = String.Format("{0}/{1}",
                        applicationPath,
                        "backend"
                    );
                    backendHtml = backendHtml.Replace("__PUBLIC_URL__", publicUrl);
                    backendHtml = backendHtml.Replace("__APPLICATION_PATH__", applicationPath);
                    await context.Response.WriteAsync(backendHtml);
                } else
                {
                    await next();
                }
                
            });

            





            config.MapHttpAttributeRoutes();
            config.EnsureInitialized();
        }
    }
}
