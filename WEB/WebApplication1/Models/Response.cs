﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;

namespace ASECL.Models
{
    public class BaseResponse
    {
        public int ErrCode { get; set; }
        public string ErrMessage { get; set; }

    }
    public class ListResponse<T>: BaseResponse
    {
        public IList<T> Items;
        public ListResponse(List<T> Items)
        {
            this.ErrCode = 200;
            this.Items = Items;
        }
    }

    public class ResultResponse<T>: BaseResponse
    {
        public T Result;
        public ResultResponse(T Result)
        {
            this.ErrCode = 200;
            this.Result = Result;
        }
    }

    public class ErrorResponse: BaseResponse
    {
        public ErrorResponse(string Message, int errCode = 400)
        {
            this.ErrCode = errCode;
            this.ErrMessage = Message;
                
        }
    }
}