﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASECL.Models;
using System.Web.Helpers;

namespace ASECL.Controllers
{

    [RoutePrefix("api/admins")]
    public class AdminController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult getList()
        {
            validToken();
            List<Admin> Items = new List<Admin>();
            Items = Db.Admin.ToList().Select(x => new Admin
            {
                Id = x.Id,
                Name = x.Name,
                Username = x.Username,
                Role = x.Role,
                Password = "",
                CreatedAt = x.CreatedAt,
                UpdatedAt = x.UpdatedAt,
                IsDisabled = x.IsDisabled
            }).ToList();

            return Json(new ListResponse<Admin>(Items));
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult add([FromBody] Admin data)
        {

            validToken();
            if ( Db.Admin.Any(x => x.Username == data.Username) )
            {
                return Json(new ErrorResponse("帳號重覆"));
            }
            data.Password = Crypto.SHA256(data.Password);
            data.CreatedAt = DateTime.Now;
            data.UpdatedAt = DateTime.Now;
            Db.Admin.Add(data);
            Db.SaveChanges();
            return Json(new ResultResponse<Admin>(data));
        }

        [Route("{id}")]
        [HttpPut]
        public IHttpActionResult update(int id, [FromBody] Admin data)
        {
            validToken();
            bool isUpdatePwd = !data.Password.Equals("");
            try
            {
                Admin dbData = Db.Admin.Where(x => x.Id == id).First();
                dbData.Name = data.Name;
                if (isUpdatePwd )
                {
                    dbData.Password = Crypto.SHA256(data.Password);
                }
                dbData.Role = data.Role;
                dbData.IsDisabled = data.IsDisabled;
                dbData.UpdatedAt = DateTime.Now;
                Db.SaveChanges();
                return Json(new ResultResponse<Admin>(dbData));
            } catch ( ArgumentNullException err )
            {
                return Json(new ErrorResponse("查無資料"));
            }
            

            
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult delete(int id)
        {
            validToken();
            try
            {
                Admin dbData = Db.Admin.Where(x => x.Id == id).FirstOrDefault();
                Db.Admin.Remove(dbData);
                Db.SaveChanges();
                return Json(new ResultResponse<Admin>(dbData));
            } catch ( ArgumentNullException )
            {
                return Json(new ErrorResponse("查無資料"));
            }
            
        }
    }
}